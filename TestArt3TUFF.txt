﻿Art. 3 


Definizioni


1. I termini bosco, foresta e selva sono equiparati.


2. Si definiscono:
a) patrimonio forestale nazionale: l'insieme dei boschi, di cui ai commi 3 e 4, e delle aree assimilate a bosco, di cui all'articolo 4, radicati sul territorio dello Stato, di proprietà pubblica e privata;
b) gestione forestale sostenibile o gestione attiva: insieme delle azioni selvicolturali volte a valorizzare la molteplicità delle funzioni del bosco, a garantire la produzione sostenibile di beni e servizi ecosistemici, nonché una gestione e uso delle foreste e dei terreni forestali nelle forme e ad un tasso di utilizzo che consenta di mantenere la loro biodiversità, produttività, rinnovazione, vitalità e potenzialità di adempiere, ora e in futuro, a rilevanti funzioni ecologiche, economiche e sociali a livello locale, nazionale e globale, senza comportare danni ad altri ecosistemi;
c) pratiche selvicolturali: i tagli, le cure e gli interventi volti all'impianto, alla coltivazione, alla prevenzione di incendi, al trattamento e all'utilizzazione dei boschi e alla produzione di quanto previsto alla lettera d);
d) prodotti forestali spontanei non legnosi: tutti i prodotti di origine biologica ad uso alimentare e ad uso non alimentare, derivati dalla foresta o da altri terreni boscati e da singoli alberi, escluso il legno in ogni sua forma;
e) sistemazioni idraulico-forestali: gli interventi e le opere di carattere intensivo ed estensivo attuati, anche congiuntamente, sul territorio, al fine di stabilizzare, consolidare e difendere i terreni dal dissesto idrogeologico e di migliorare l'efficienza funzionale dei bacini idrografici e dei sistemi forestali;
f) viabilità forestale e silvo-pastorale: la rete di strade, piste, vie di esbosco, piazzole e opere forestali aventi carattere permanente o transitorio, comunque vietate al transito ordinario, con fondo prevalentemente non asfaltato e a carreggiata unica, che interessano o attraversano le aree boscate e pascolive, funzionali a garantire il governo del territorio, la tutela, la gestione e la valorizzazione ambientale, economica e paesaggistica del patrimonio forestale, nonché le attività di prevenzione ed estinzione degli incendi boschivi;
g) terreni abbandonati: fatto salvo quanto previsto dalle normative regionali vigenti, i terreni forestali nei quali i boschi cedui hanno superato, senza interventi selvicolturali, almeno della metà il turno minimo fissato dalle norme forestali regionali, ed i boschi d'alto fusto in cui non siano stati attuati interventi di sfollo o diradamento negli ultimi venti anni, nonché i terreni agricoli sui quali non sia stata esercitata attività agricola da almeno tre anni, in base ai principi e alle definizioni di cui al regolamento (UE) n. 1307/2013 del Parlamento europeo e del Consiglio del 17 dicembre 2013 e relative disposizioni nazionali di attuazione, ad esclusione dei terreni sottoposti ai vincoli di destinazione d'uso;
h) terreni silenti: i terreni agricoli e forestali di cui alla lettera g) per i quali i proprietari non siano individuabili o reperibili a seguito di apposita istruttoria;
i) prato o pascolo permanente: le superfici non comprese nell'avvicendamento delle colture dell'azienda da almeno cinque anni, in attualità di coltura per la coltivazione di erba e altre piante erbacee da foraggio, spontanee o coltivate, destinate ad essere sfalciate, affienate o insilate una o più volte nell'anno, o sulle quali è svolta attività agricola di mantenimento, o usate per il pascolo del bestiame, che possono comprendere altre specie, segnatamente arbustive o arboree, utilizzabili per il pascolo o che producano mangime animale, purché l'erba e le altre piante erbacee da foraggio restino predominanti;
l) prato o pascolo arborato: le superfici in attualità di coltura con copertura arborea forestale inferiore al 20 per cento, impiegate principalmente per il pascolo del bestiame;
m) bosco da pascolo: le superfici a bosco destinate tradizionalmente anche a pascolo con superficie erbacea non predominante;
n) arboricoltura da legno: la coltivazione di impianti arborei in terreni non boscati o soggetti ad ordinaria lavorazione agricola, finalizzata prevalentemente alla produzione di legno a uso industriale o energetico e che è liberamente reversibile al termine del ciclo colturale;
o) programmazione forestale: l'insieme delle strategie e degli interventi volti, nel lungo periodo, ad assicurare la tutela, la valorizzazione, la gestione attiva del patrimonio forestale o la creazione di nuove foreste;
p) attività di gestione forestale: le attività descritte nell'articolo 7, comma 1;
q) impresa forestale: impresa iscritta nel registro di cui all'articolo 8 della legge 29 dicembre 1993, n. 580, che esercita prevalentemente attività di gestione forestale, fornendo anche servizi in ambito forestale e ambientale e che risulti iscritta negli elenchi o negli albi delle imprese forestali regionali di cui all'articolo 10, comma 2;
r) bosco di protezione diretta: superficie boscata che per la propria speciale ubicazione svolge una funzione di protezione diretta di persone, beni e infrastrutture da pericoli naturali quali valanghe, caduta massi, scivolamenti superficiali, lave torrentizie e altro, impedendo l'evento o mitigandone l'effetto;
s) materiale di moltiplicazione: il materiale di cui all'articolo 2, comma 1, lettera a), del decreto legislativo 10 novembre 2003, n. 386. 


3. Per le materie di competenza esclusiva dello Stato, sono definite bosco le superfici coperte da vegetazione forestale arborea, associata o meno a quella arbustiva, di origine naturale o artificiale in qualsiasi stadio di sviluppo ed evoluzione, con estensione non inferiore ai 2.000 metri quadri, larghezza media non inferiore a 20 metri e con copertura arborea forestale maggiore del 20 per cento. 


4. Le regioni, per quanto di loro competenza e in relazione alle proprie esigenze e caratteristiche territoriali, ecologiche e socio-economiche, possono adottare una definizione integrativa di bosco rispetto a quella dettata al comma 3, nonché definizioni integrative di aree assimilate a bosco e di aree escluse dalla definizione di bosco di cui, rispettivamente, agli articoli 4 e 5, purché non venga diminuito il livello di tutela e conservazione così assicurato alle foreste come presidio fondamentale della qualità della vita.